The applicability of deep and conventional machine learning techniques for sepsis onset detection.
==============================

The technical implementation for our masters' thesis

Abstract:
Sepsis is a common and hazardous condition, being a leading cause of mortality within critical care settings. Timely diagnosis is crucial for effective treatment and thus patient outcome. Sepsis causes symptoms consistent with benign conditions, fostering a difficult environment for definitive clinician diagnosis. Current diagnostic practices rely upon composite scoring techniques, despite their effectiveness remaining hotly debated. Diagnosis via detection of chemical mediators promises to be a compelling replacement, however these methods remain laboratory bound, yet to see widespread clinical application. Our work examines the applicability of machine learning techniques for sepsis onset prediction - preempting diagnosis. We examine both, conventional and deep architectures' ability to identify sepsis from patient populations. With further consideration given to how feature engineering techniques can influence machine learning model performance. This is an inherently difficult problem, with none of our resulting models proving dominance. Furthermore, architectural differences did not translate to radically different model performance. Rather, feature engineering methods fostered the most significant performance diversity.


Project Organization
------------

    ├── LICENSE
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- Supporting documentation, including quick start guide. 
    │
    ├── models             <- All model data.
    │
    ├── notebooks          <- Jupyter notebooks. Where all technical implementation is. Each containing its own figures.
    │
    └── requirements.txt   <- The requirements file.

--------
